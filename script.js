
const MAX = 256;

//Funzioni di conversione
(function(){

    var ConvertBase = function (num) {
        return {
            from : function (baseFrom) {
                return {
                    to : function (baseTo) {
                        return parseInt(num, baseFrom).toString(baseTo);
                    }
                };
            }
        };
    };
        
    // binary to decimal
    ConvertBase.bin2dec = function (num) {
        return ConvertBase(num).from(2).to(10);
    };
    
    // binary to hexadecimal
    ConvertBase.bin2hex = function (num) {
        return ConvertBase(num).from(2).to(16);
    };
    
    // decimal to binary
    ConvertBase.dec2bin = function (num) {
        return ConvertBase(num).from(10).to(2);
    };
    
    // decimal to hexadecimal
    ConvertBase.dec2hex = function (num) {
        return ConvertBase(num).from(10).to(16);
    };
    
    // hexadecimal to binary
    ConvertBase.hex2bin = function (num) {
        return ConvertBase(num).from(16).to(2);
    };
    
    // hexadecimal to decimal
    ConvertBase.hex2dec = function (num) {
        return ConvertBase(num).from(16).to(10);
    };
    
    this.ConvertBase = ConvertBase;
    
})(this);

function getBaseLog(x, y)  //Calcolo del logaritmo
{
  return Math.log(y) / Math.log(x);
}


//Inizio mio codice

function set() //Settaggi da applicare al caricamento dell'app
{
   document.getElementById('ip').style.border = "2px solid #000000";
   document.getElementById('subnet').style.border = "2px solid #000000";
   document.getElementById('numhost').style.border = "2px solid #000000";
}

function reset()  //resetta le impostazioni dopo un errore
{
   //alert("reset");
   document.getElementById("error").innerHTML ="";
   document.getElementById("host").innerHTML ="";
   document.getElementById("magicnumber").innerHTML ="";
   document.getElementById('subnet').style.border = "2px solid #000000";
   document.getElementById("ip").style.border = "2px solid #000000";
   document.getElementById('info').style.display = "block";
   document.getElementById("controlla").disabled = false;
  return 0;
}

function viewinfo(control)
{
	if(control==true)
		document.getElementById('info').style.display = "block";
	else
		document.getElementById('info').style.display = "none";
}


function errorip(code)  //Con la variabile code definisco se l'errore riguarda l'ip(1), la mask(2) o il coglione(3)
{
  if(code==2)
  {
    var ip=document.getElementById("ip").value;
	document.getElementById("error").innerHTML ="SUBNET MASK NON CORRETTA";
	document.getElementById("subnet").value="";
	//document.getElementById("ip").value="<font color="red">This is some text!</font>"
	//alert(ip.lastIndexOf("/"));
	
  }
  //document.getElementById('ip').style.border = "2px solid #ff0000";
  document.getElementById('info').style.display = "none";
  document.getElementById("controlla").disabled = true;
  return -1;
}

function errorsub()  //Funzione d'errore nella subnet
{
  //alert("Subnet mask non corretta");
  document.getElementById("error").innerHTML ="SUBNET MASK NON CORRETTA";
  document.getElementById('subnet').style.border = "2px solid #ff0000";
  document.getElementById('info').style.display = "none";
  document.getElementById("controlla").disabled = true;
  return -1;
}

function seiuncoglione()  //Funzione d'errore nella subnet
{
  //alert("La cosa che hai digitato non ha senso \nSei un coglione\n");
  document.getElementById("error").innerHTML ="LA COSA CHE HAI DIGITATO NON HA SENSO";
  document.getElementById('subnet').style.border = "2px solid #ff0000";
  document.getElementById('info').style.display = "none";
  document.getElementById("controlla").disabled = true;
  return -1;
}

/*
function numtobin(primo,secondo,terzo,quarto) //Converte i 4 "pezzi" di numero in binario e ritorna un'unica stringa
{
  var primobin=ConvertBase.dec2bin(primo);
  var secondobin=ConvertBase.dec2bin(secondo);
  var terzobin=ConvertBase.dec2bin(terzo);
  var quartobin=ConvertBase.dec2bin(quarto);
  var visual=primobin+"."+secondobin+"."+terzobin+"."+quartobin;
  var numero=primobin+secondobin+terzobin+quartobin;
  return numero;
}
*/

function controlli()  //Controlli all'uscita del secondo box (Subnet mask)
{
    //Ricezione della subnet e raggruppamento a gruppi di 3 e creazione di una subnet senza la divisione dei punti
  var x = document.getElementById("subnet").value;
  var primo= x.split('.')[0];
  var secondo= x.split('.')[1];
  var terzo= x.split('.')[2];
  var quarto= x.split('.')[3];
  var err=x.split('.')[4];
  var numero=primo+secondo+terzo+quarto;
  
  //Primo controllo se la subnet è valida (cioè nella forma n.n.n.n e che non contenga lettere)
  if(quarto==undefined||err!=undefined||isNaN(numero)||primo<0)
  {
    return errorsub();  //retunrn -1 con messaggio d'errore
  }
  
  //Conversione dei "pezzi" di numero in binario
  var primobin=ConvertBase.dec2bin(primo);
  var secondobin=ConvertBase.dec2bin(secondo);
  var terzobin=ConvertBase.dec2bin(terzo);
  var quartobin=ConvertBase.dec2bin(quarto);
  var visual=primobin+"."+secondobin+"."+terzobin+"."+quartobin;
  numero=primobin+secondobin+terzobin+quartobin;
  
  //document.getElementById("demo").innerHTML =visual;
  
  //Controllo che i singoli blocchi non superino 8 bit
  if(primobin.length>8||secondobin.length>8||terzobin.length>8||quartobin.length>8)
     return errorsub();
  
  //Controllo che ci sia almeno un host
  if(quartobin[7]==1&&quartobin[7]!=undefined)
     return seiuncoglione();
  
  //Ricerca di ulteriori errori nel numero binario della subnet
  var i;
  err=false;
  for (i = 0; i < numero.length; i++) { 
    if(numero[i]==0)
       err=true;
    else if(err==true)
       return errorsub();
   }
  

  //Se fino a qui non ho riscontrato errori resetto i possibili precedenti
  reset();
  
  return subnetslesc(numero);
}

function protocollo() //riconoscimento protocollo
{
   var ip=document.getElementById("ip").value;
   if(ip.search("://")<0)
	   document.getElementById("protocollo").innerHTML ="";
   else
   {
	   document.getElementById("protocollo").innerHTML ="Il protocollo è <b>"+ip.substring(0, ip.search("://"))+"</b>";
   }
   //alert(ip);
	
}

/*
function controlip(num)
{
 if()
 if(0<num<32)
}
*/

function controlmask(num)
{
  //alert(num);
  if(isNaN(num))
  {
    errorip(2);
	return -1;
  }
  return 0;
}

function erasemask() //Azzera la maschera /n per aggiornarla con quella nuova /n1
{
  var ip=document.getElementById("ip").value;
  if(ip.lastIndexOf("/")>=0)
      document.getElementById("ip").value = ip.slice(0,ip.lastIndexOf("/"));
}

function outbox1() //Sei uscito dal primo inputbox
{
  //alert("Fuori dal box1");
  reset();
  var ip=document.getElementById("ip").value;
  //alert();
  var mask=ip.substring(ip.lastIndexOf("/")+1,ip.length);
  
  if(ip=="")
  {
	document.getElementById("subnet").value = "";
	return -1;
  }
  
  viewinfo(false);
  
  //alert(mask);
  if(controlmask(mask)>=0)
  {
     //alert("sono andato avanti");
     //alert(mask);
     if(mask>32)
       alert("ok");
     //controlip(mask);
     //alert(mask);
     //alert(Number.isInteger(mask));
	 //protocollo(ip);
     subnetdot(mask);
     //controlli();
     //alert(mask);
  }
}

function outbox2() //Sei uscito dal secondo inputbox
{
  if(document.getElementById("subnet").value!="") //Controllo che l'utente dopo essere uscito abbia inserito qualcosa
  {
     //alert("sas");
     erasemask();
     var check=controlli(); //eseguo i controlli
     if(check>=0) //controllo non vi siano stati errori
        document.getElementById("ip").value=document.getElementById("ip").value+"/"+check;
  }
  else
    reset();
  
  viewinfo(false);

}

function magicnumber(num)  //Calcolo del magic number
{
  //alert(num);
  var magic=MAX-num;  //Radice del magic number
  document.getElementById("magicnumber").innerHTML = "Il <b>magicnumber</b> &egrave: <b>"+magic+"</b> (2<sup>"+getBaseLog(2,magic)+"</sup>)";
  return magic;
}

function subnetslesc(num)  //Calcolo della subnet in formato /num
{
  //alert(num);
  var sub=0;
  for (i = 0; i < num.length; i++)
  {
    if(num[i]==1)
	  sub++;
  }
  //document.getElementById("subnetmask").innerHTML ="La <b>subnet</b> in formato / &egrave <b>/"+sub+"</b>";
  return sub;
}

function subnetdot(num)
{
  //alert(num);
  var x=[];
  document.getElementById("subnet").value="";  //Azzero la vecchia subnet
  for(var i=0;i<4;i++)
  {
    x[i]=0;
	if(num>=8)
	{
	  num=num-8;
	  x[i]=255;
	}
	else if(num!=0)
	      {
		    var min=8-num;
			var y=7;
			while(y>=min)
			{
			  x[i]=x[i]+Math.pow(2, y);
			  y--;
			}
			num=0;
			  
		  }
	
	//alert(i);
  }
  //alert("OK");
 // document.getElementById("demo2").value="CIAO";
  //alert(x[0]);
  //document.getElementById("demo").style.display = "block";
  document.getElementById("subnet").value=x[0]+"."+x[1]+"."+x[2]+"."+x[3];
  //alert("Finito");
  //alert("subnetdot");
  
}


function host(numero) //Calcola il numero degli host
{
  var num1=numero.replace(/[^1]/g,"").length;
  var numhost=(Math.pow(2, 32-num1))-2;
  document.getElementById("host").innerHTML = "Il numero di <b>Host disponibili</b> con questa subnet sono: <b>" + numhost+"</b>";
  return numhost;
}

function main()  //Funzione eseguita al click di controlla, corpo principale del programma
{

  //Ricezione della subnet e raggruppamento a gruppi di 3 e creazione di una subnet senza la divisione dei punti
  var x = document.getElementById("subnet").value;
  var primo= x.split('.')[0];
  var secondo= x.split('.')[1];
  var terzo= x.split('.')[2];
  var quarto= x.split('.')[3];
  var err=x.split('.')[4];
  var numero=primo+secondo+terzo+quarto;
    
  //Conversione dei "pezzi" di numero in binario
  var primobin=ConvertBase.dec2bin(primo);
  var secondobin=ConvertBase.dec2bin(secondo);
  var terzobin=ConvertBase.dec2bin(terzo);
  var quartobin=ConvertBase.dec2bin(quarto);
  var visual=primobin+"."+secondobin+"."+terzobin+"."+quartobin;
  numero=primobin+secondobin+terzobin+quartobin;
  
  document.getElementById("demo").innerHTML =visual;  

  //Se fino a qui non ho riscontrato errori resetto i possibili precedenti
  reset();
  
  viewinfo(true);
   
  host(numero); 
  
  protocollo();
  
  //Controlla se c'è un 1 all'interno delle stringhe
  if(quartobin.includes(1))
     magicnumber(quarto);
  else if(terzobin.includes(1))
     magicnumber(terzo);
  else if(secondobin.includes(1))
     magicnumber(secondo);
  else if(primobin.includes(1))
     magicnumber(primo);
  else
     document.getElementById("magicnumber").innerHTML = "Il <b>magicnumber</b> non ha senso";
  
}

function run() //Esegue main() senza errori
{
	main();
	return false;
}

/*
function invio()
{
   alert("Ciao2");
   main();
}
*/